# coding: utf-8
# 时间轮TimingWheel实现
# 高效定时器的实现方式：时间轮（环形队列+链表）、小顶堆（最快超时+调整）

#python自带的环形队列（设置了maxlen的deque）
import collections
deq=collections.deque(maxlen=3)
deq.append(1)
deq.append(2)
deq.append(3)
deq.append(4)
print(deq)

#环形队列使用【求余】进行遍历
for i in range(10):
    l=len(deq)
    print(deq[i%l])
