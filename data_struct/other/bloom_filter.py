# coding: utf-8
# 布隆过滤器实现
# BloomFilter(BF)是一种特殊的哈希表，它不记录value，只记录key是否曾经保存过值
# 如果BF说没有存过，那就是的确没有；如果BF说存过，那么可能没有，也可能有。
