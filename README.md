# ConciseAlgoCollection

#### 介绍
算法简洁实现收集，简洁且正确，方便记忆。

#### 软件架构
使用语言：Python

#### 目录
| 序号 | 标题 | 地址                                                                              |
| ---- | ---- | --------------------------------------------------------------------------------- |
| 1    | 快排 | https://gitee.com/coderkim/ConciseAlgoCollection/blob/master/sorting/quick_sort.py |
| 2    | 归并排序 | https://gitee.com/coderkim/ConciseAlgoCollection/blob/master/sorting/merge_sort.py |
| 3    | 插入排序 | https://gitee.com/coderkim/ConciseAlgoCollection/blob/master/sorting/insertion_sort.py |
| 4    | 二分查找 |   https://gitee.com/coderkim/ConciseAlgoCollection/blob/master/AlgoPicture/binary_search.py  |
|  |  |  |
|  |  |  |

#### 参考文章
[常见的10种排序算法](https://www.cnblogs.com/flyingdreams/p/11161157.html)
